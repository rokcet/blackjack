let Fraction = require('fraction.js');
const VALUES = [11,2,3,4,5,6,7,8,9,10,10,10,10];
const NVALUES = 13;
const ACE = 11;

const HIT_SOFT_17 = true;

let probCache = {};

function memo(runTotal, soft){
  //check
  let key = runTotal + ':' + soft;
  if(key in probCache)
    return probCache[key];

  //divide
  let subProbs = VALUES.map(v => {
    //we need to figure out if we're dealing with soft
    let subNext = runTotal + v;
    let subSoft = soft;

    if(v === ACE)
      subSoft += 1;

    if (subSoft > 0 && subNext > 21) {
      subNext -= 10;
      subSoft -= 1;
    }

    let subKey = subNext + ':' + subSoft;

    if(subKey in probCache)
      return probCache[subKey];

    if(subNext >= 18){
      let res = Array(30).fill(new Fraction(0));
      res[subNext] = new Fraction(1);
      probCache[subKey] = res;
      return res;
    }
    else if(subNext === 17){
      if (subSoft > 0 && HIT_SOFT_17)
        return memo(subNext, subSoft);
      else {
        let res = Array(30).fill(new Fraction(0));
        res[subNext] = new Fraction(1);
        probCache[subKey] = res;
        return res;
      }
    }
    else if(subNext <= 16)
      return memo(subNext, subSoft);
  });

  //merge
  //add all the sub probs //then divide by 13
  subProbs = subProbs.reduce((accumulator, currentValue) => {
    return accumulator.map((a, i) =>
        a.add(currentValue[i])
    );
  }, Array(30).fill(new Fraction(0)));

  //then divide by 13
  subProbs = subProbs.map(s => s.div(13));

  //save
  probCache[key] = subProbs;
  return subProbs;

}

const bestFunc = (start) => {
  console.log('Dealer starts with ' + start);

  let probs = memo(start, start === 11? 1: 0);

  for(let i = 17; i < 22; i++)
    console.log(i + ':\t' + probs[i].toString());

  let bust = new Fraction(0);
  for(let i = 22; i < 27; i++)
    bust = bust.add(probs[i]);

  console.log('bust:\t' + bust.toString());
};

[11,2,3,4,5,6,7,8,9,10].forEach(bestFunc);


